package org.nedj.classes;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;




@NamedQueries(
		{
		@NamedQuery(name="Commune.byPostalCode",query="select c from Commune c where c.codePostal=:codePostal"),
		})

@XmlRootElement(name="commune")
@XmlAccessorType(XmlAccessType.FIELD)
@Entity
@Access(AccessType.FIELD)
public class Commune {
	
	// je me contente que des champs id,nom, codePostal vu que j'utilise ceux la
	@XmlAttribute(name ="id")
	@Id @GeneratedValue(strategy= GenerationType.AUTO)
	private long id;
	
	@XmlElement(name="nom")
	@Column(name="nom", length = 80 )
	private String nom;
	
	@XmlElement(name="codePostal")
	@Column(name="code_postal" ,length=40)
	private String codePostal;
	
	
	
	public Commune(long id, String name) {
	
		this.id = id;
		this.nom= name;
	}
	
	public Commune( String name) {
		
		this.nom = name;
	}
	
public Commune( long id) {
		
		this.id=id;
	}
	
	public Commune() {

	}
	

	public Commune(String nom, String codePostal) {
		this.codePostal = codePostal;
		this.nom= nom;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String name) {
		this.nom = name;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	@Override
	public String toString() {
		return "Commune [id=" + id + ", nom=" + nom + ", codePostal=" + codePostal + "]";
	}

	
	

	
	
}
