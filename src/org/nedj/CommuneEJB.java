package org.nedj;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.nedj.classes.Commune;



@Stateless
public class CommuneEJB {

	@PersistenceContext(unitName="tp-jpa-eclipselink-create")
	private EntityManager em; 
	 
	public long create(Commune commune)
	{
		em.persist(commune);
		return commune.getId();
	}

	public Commune findById(long id) {
		return em.find(Commune.class, id);
	}

	public Commune findByCodePostal(String codePostal) {
		
		Query query= em.createNamedQuery("Commune.byPostalCode");
		query.setParameter("codePostal", codePostal);
		return (Commune) query.getSingleResult();
	}

	public long  update(Commune commune) {
		//existing element in DB
		em.merge(commune);
		return commune.getId();
	}

	public boolean delete(String codePostal) {
		Query query= em.createNamedQuery("Commune.byPostalCode");
		query.setParameter("codePostal", codePostal);
		Commune commune=(Commune) query.getSingleResult();
		if(commune!=null)
		{
			em.remove(commune);
			return true;
		}
		return false;
		
	}
}
