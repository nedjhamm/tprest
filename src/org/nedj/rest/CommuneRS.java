package org.nedj.rest;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.nedj.CommuneEJB;
import org.nedj.classes.Commune;

@Path("commune")

public class CommuneRS {

	@EJB
	private CommuneEJB communeEJB;
	
	@GET @Path("ping")
	public String ping()
	{
		return "pong";
	} 
	
	
	/*@GET @Path("add/{x}/{y}")
	public int add(@PathParam("x") int x,@PathParam("y") int y)
	{
		return x+y;
	}*/
	
/*	@GET @Path("{id}")
	@Produces(MediaType.TEXT_XML)
	public Commune findById(@PathParam("id") long id )
	{
		Commune paris=new Commune (id,"Paris");

		return paris;
	}*/
	
	@GET @Path("{id}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
	//@Produces(MediaType.TEXT_XML)
	public Response findByIdS2(@PathParam("id") long id )
	{
		
		Commune commune=communeEJB.findById(id);//new Commune (id,"Paris");
		if(commune!=null)
		return  Response.ok(commune).build();
		else
			return Response.status(Status.NOT_FOUND).build();
	}
	
	
/*	@GET @Path("{codePostal}")
	@Produces({ MediaType.TEXT_XML})
	public Commune findByCodePostalS2(@PathParam("codePostal") String codePostal )
	{
		Commune paris=communeEJB.findByCodePostal(codePostal);//new Commune (id,"Paris");

		return paris;
	}*/
	
	
	//html file addCommune.html
	@POST @Path("create")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response createCommune(@FormParam("name") String nom, @FormParam("postalCode") String codePostal)
	{
		Commune commune=new Commune(nom, codePostal);
		long id=  communeEJB.create(commune);
		if(commune!=null)
			return  Response.ok(id).build();
			else
				return Response.status(Status.NOT_FOUND).build();
		
	}
	
	@PUT @Path("{codePostal}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response updateCommune(@PathParam("codePostal") String codePostal,@FormParam("name") String nom)
	{
		Commune commune=communeEJB.findByCodePostal(codePostal);
		if(commune!=null)
		{
			commune.setNom(nom);
			commune.setCodePostal(codePostal);
			communeEJB.update(commune);
			return  Response.ok(codePostal).build();
		}
		else
		{
			return Response.status(Status.NOT_FOUND).build();
		}
	}
	
	@DELETE @Path("delete/{codePostal}")
	public Response deleteCommune(@PathParam("codePostal") String codePostal)
	{
		if(communeEJB.delete(codePostal))
		{
			return  Response.ok(codePostal).build();
		}
		else
			return Response.status(Status.NOT_FOUND).build();
	}
	
}
 