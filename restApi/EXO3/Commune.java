package EXO3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "commune")
@XmlAccessorType(XmlAccessType.FIELD)

public class Commune {

	@XmlElement(name = "id")
	private long id;
	@XmlElement(name = "name")
	private String name;
	
	
	public Commune(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public Commune() {
		super();
	}
	

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Commune [id=" + id + ", name=" + name + "]";
	}
	

	
	
}
