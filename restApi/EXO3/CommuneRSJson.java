package EXO3;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;


@Path("exo-3/commune-json")
public class CommuneRSJson {

	@GET @Path("{name}/{id}")
	
	public CommuneJson findById(@PathParam("id") long id,@PathParam("name") String name )
	{
		CommuneJson commune=new CommuneJson (id,name);
		return commune;
	}
	
}
