package EXO3;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;



// I use the same AppRest I made in EXO2


@Path("exo-3/commune")
public class CommuneRS {


	@GET @Path("{name}/{id}")
	@Produces(MediaType.TEXT_XML)
	public Commune findById(@PathParam("id") long id,@PathParam("name") String name )
	{
		Commune commune=new Commune (id,name);
		return commune;
	}
	
}
