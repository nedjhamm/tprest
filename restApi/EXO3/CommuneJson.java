package EXO3;

public class CommuneJson {
	private long id;
	private String name;
	
	
	public CommuneJson(long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public CommuneJson() {
		super();
	}
	

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Commune [id=" + id + ", name=" + name + "]";
	}
	


}
