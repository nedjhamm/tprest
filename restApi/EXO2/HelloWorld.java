package EXO2;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


@Path("exo-2")
public class HelloWorld {
	
	@GET @Path("hello-world")
	public String helloWorld()
	{
		return "Hello world !";
	}
	

	@GET @Path("{id}")
	public Response enterId(@PathParam("id") long id )
	{
		if(id%2==0)
			return Response.status(Status.OK).build();
		else
		{
			return Response.status(Status.NOT_FOUND).build();
		}
		//return "You entered: "+id;
	}

}
